<?php
/* Smarty version 4.1.0, created on 2023-02-06 14:59:42
  from '/app/www/design/backend/templates/components/bottom_panel/icons/bp-close.svg' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.1.0',
  'unifunc' => 'content_63e0ebae8f9bd7_45024273',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'fe1c3c2244b3c771887aeb8deb1b7fd0f64815a3' => 
    array (
      0 => '/app/www/design/backend/templates/components/bottom_panel/icons/bp-close.svg',
      1 => 1675156538,
      2 => 'backend',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_63e0ebae8f9bd7_45024273 (Smarty_Internal_Template $_smarty_tpl) {
?><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor"
    class="bp-close__icon">
    <path
        d="M16.644 8.966l-3.025 3.024 3.025 3.025a1.152 1.152 0 0 1-1.629 1.629l-3.025-3.025-3.024 3.025a1.148 1.148 0 0 1-1.628 0 1.152 1.152 0 0 1 0-1.63l3.024-3.024-3.025-3.024a1.152 1.152 0 0 1 1.629-1.629l3.024 3.025 3.025-3.025a1.152 1.152 0 0 1 1.629 1.629z" />
</svg><?php }
}
