<?php
/* Smarty version 4.1.0, created on 2023-02-06 14:30:15
  from '0a2fa8ffc7914860826e395f6897fd39ad5af7c6' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.1.0',
  'unifunc' => 'content_63e0e4c74a0071_82148475',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_63e0e4c74a0071_82148475 (Smarty_Internal_Template $_smarty_tpl) {
\Tygh\Languages\Helper::preloadLangVars(array('get_social'));
?>
<div class="ty-social-link-block"><h3 class="ty-social-link__title"><?php echo $_smarty_tpl->__("get_social");?>
</h3>

<div class="ty-social-link facebook">
    <a href="http://www.facebook.com/pages/CS-Cart/156687676230"><span class="ty-icon ty-icon-facebook ty-icon-moon-facebook"></span></a>
</div>

<div class="ty-social-link instagram">
    <a href="https://www.instagram.com"><span class="ty-icon ty-icon-instagram ty-icon-moon-instagram"></span></a>
</div>

<div class="ty-social-link twitter">
    <a href="https://twitter.com/cscart"><span class="ty-icon ty-icon-twitter ty-icon-moon-twitter"></span></a>
</div>

<div class="ty-social-link youtube">
    <a href="https://www.youtube.com/user/cscartvideos"><span class="ty-icon ty-icon-youtube ty-icon-moon-youtube"></span></a>
</div></div>
<?php }
}
