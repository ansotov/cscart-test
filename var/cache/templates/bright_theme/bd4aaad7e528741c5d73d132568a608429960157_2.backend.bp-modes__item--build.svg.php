<?php
/* Smarty version 4.1.0, created on 2023-02-06 14:59:39
  from '/app/www/design/backend/templates/components/bottom_panel/icons/bp-modes__item--build.svg' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '4.1.0',
  'unifunc' => 'content_63e0ebabe79aa9_29506158',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bd4aaad7e528741c5d73d132568a608429960157' => 
    array (
      0 => '/app/www/design/backend/templates/components/bottom_panel/icons/bp-modes__item--build.svg',
      1 => 1675156538,
      2 => 'backend',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_63e0ebabe79aa9_29506158 (Smarty_Internal_Template $_smarty_tpl) {
?><svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor"
    class="bp-modes__item-icon">
    <path d="M5 8h22v10.75l-2-1.25V10H7v11h9.75l.25 2H5" />
    <path
        d="M25.883 26.237L23.59 22.27l2.543-.372a.478.478 0 0 0 .189-.875l-6.834-4.38a.478.478 0 0 0-.735.425l.377 8.104a.478.478 0 0 0 .853.275l1.594-2.015 2.291 3.966a.478.478 0 0 0 .653.175l1.186-.684a.478.478 0 0 0 .175-.652z" />
</svg><?php }
}
